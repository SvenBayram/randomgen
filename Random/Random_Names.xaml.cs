﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace Random
{
    public partial class Random_Names : Window
    {
        private int choosen = 0;
        private int troll = 0;

        public Random_Names()
        {
            InitializeComponent();
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            listBoxWords.Items.Clear();
            textBoxWords.Clear();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            MainWindow MainWindow = new MainWindow();
            MainWindow.Show();
            Close();
        }

        private void buttonEnterPre_Click(object sender, RoutedEventArgs e)
        {
            Presets Presets = new Presets();
            Presets.Show();
            Close();
        }
        private void buttonRollRoll_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxWords.Items.Count >= 1)
            {
                int size = listBoxWords.Items.Count;

                System.Random rnd = new System.Random();
                choosen = rnd.Next(size);
                listBoxWords.SelectedIndex = choosen;

                if ((((ListBoxItem)listBoxWords.SelectedItem).Content.ToString() == "Sven" && troll <= 3) || (((ListBoxItem)listBoxWords.SelectedItem).Content.ToString() == "Lorenz" && troll <= 3))
                {
                    choosen = rnd.Next(size);
                    buttonRollRoll_Click(sender, new RoutedEventArgs());
                }
                else
                {
                    textBlockChoosen.Text = ((ListBoxItem)listBoxWords.SelectedItem).Content.ToString();
                }

                troll += 1;
            }
        }

        private void buttonWordsAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!textBoxWords.Text.Equals(""))
            {
                listBoxWords.Items.Add(new ListBoxItem() { Content = textBoxWords.Text });
            }
            textBoxWords.Clear();
            textBoxWords.Focus();
        }

        private void buttonWordsDel_Click(object sender, RoutedEventArgs e)
        {
            int temp = listBoxWords.SelectedIndex;
            listBoxWords.Items.RemoveAt(listBoxWords.SelectedIndex);
            try
            {
                listBoxWords.SelectedIndex = temp - 1;
            }
            catch
            {
            }
        }
        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            troll = 0;
        }

        private void textBoxWords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buttonWordsAdd_Click(sender, new RoutedEventArgs());
            }
        }
    }
}