﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;

namespace Random
{
    public partial class Presets : Window
    {
        private int del = 0;

        public Presets()
        {
            InitializeComponent();

            if (!File.Exists("Saves\\preset.xml"))
            {
                DirectoryInfo di = Directory.CreateDirectory("Saves");
                XmlWriterSettings setting1 = new XmlWriterSettings();
                setting1.Indent = true;
                setting1.ConformanceLevel = ConformanceLevel.Auto;

                XmlDocument doc = new XmlDocument();

                XmlWriter writer1 = XmlWriter.Create("Saves\\preset.xml", setting1);

                try
                {
                    writer1.WriteStartDocument();
                    writer1.WriteStartElement("presetlist");
                    writer1.WriteElementString("preset", "Default List");
                    writer1.WriteEndElement();
                    writer1.WriteEndDocument();
                }
                catch (Exception e)
                {
                    if (e is IOException)
                    {
                        MessageBox.Show("Another user is already using this file.");
                    }
                    else
                    {
                        MessageBox.Show("Other Exception!");
                    }
                }

                writer1.Close();

                try
                {
                    doc.Load("Saves\\preset.xml");
                    XmlElement root = doc.DocumentElement;
                    foreach (XmlNode preset in root.ChildNodes)
                    {
                        listBoxPre.Items.Add(new ListBoxItem { Content = preset.InnerText });
                    }
                }
                catch (Exception e)
                {
                    if (e is IOException)
                    {
                        MessageBox.Show("Another user is already using this file.");
                    }
                    else
                    {
                        MessageBox.Show("Other Exception!");
                    }
                }

                XmlWriterSettings setting2 = new XmlWriterSettings();
                setting2.Indent = true;
                setting2.ConformanceLevel = ConformanceLevel.Auto;

                XmlDocument doc2 = new XmlDocument();

                XmlWriter writer2 = XmlWriter.Create("Saves\\Default List.xml", setting2);

                try
                {
                    writer2.WriteStartDocument();
                    writer2.WriteStartElement("namelist");
                    writer2.WriteElementString("name", "What?");
                    writer2.WriteElementString("name", "Where?");
                    writer2.WriteElementString("name", "When?");
                    writer2.WriteElementString("name", "Who?");
                    writer2.WriteEndElement();
                    writer2.WriteEndDocument();
                }
                catch (Exception e)
                {
                    if (e is IOException)
                    {
                        MessageBox.Show("Another user is already using this file.");
                    }
                    else
                    {
                        MessageBox.Show("Other Exception!");
                    }
                }

                writer2.Close();
            }
            else
            {
                XmlDocument doc2 = new XmlDocument();

                try
                {
                    doc2.Load("Saves\\preset.xml");
                    XmlElement root = doc2.DocumentElement;
                    foreach (XmlNode preset in root.ChildNodes)
                    {
                        listBoxPre.Items.Add(new ListBoxItem { Content = preset.InnerText });
                    }
                }
                catch (Exception ee)
                {
                    if (ee is IOException)
                    {
                        MessageBox.Show("Another user is already using this file.");
                    }
                    else
                    {
                        MessageBox.Show("Other Exception!");
                    }
                }
                listBoxPre.SelectedIndex = 0;
            }
        }

        private void buttonChoose_Click(object sender, RoutedEventArgs e)
        {
            string choosen = ("Saves\\" + ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString() + ".xml");

            Random_Names Random_Names = new Random_Names();
            Random_Names.Show();

            XmlDocument doc4 = new XmlDocument();

            try
            {
                doc4.Load("Saves\\" + ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString() + ".xml");
                XmlElement root = doc4.DocumentElement;
                foreach (XmlNode name in root.ChildNodes)
                {
                    Random_Names.listBoxWords.Items.Add(new ListBoxItem { Content = name.InnerText });
                }
            }
            catch (Exception ee)
            {
                if (ee is IOException)
                {
                    MessageBox.Show("Another user is already using this file.");
                }
                else
                {
                    MessageBox.Show("Other Exception!");
                }
            }
            buttonSave_Click(sender, new RoutedEventArgs());
            Close();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            MainWindow MainWindow = new MainWindow();
            MainWindow.Show();
            Close();
        }

        private void buttonNamesAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!textBoxNames.Text.Equals(""))
            {
                listBoxNames.Items.Add(new ListBoxItem { Content = textBoxNames.Text });
                int tempx = listBoxNames.Items.Count;
            }
            textBoxNames.Clear();

            buttonSave_Click(sender, new RoutedEventArgs());

            textBoxNames.Focus();
        }

        private void buttonNamesDel_Click(object sender, RoutedEventArgs e)
        {
            int temp = listBoxNames.SelectedIndex;
            if (listBoxNames.Items.Count > 0)
            {
                if (!(listBoxNames.Items.Count == 4 && listBoxNames.Items.GetItemAt(0).ToString() == "System.Windows.Controls.ListBoxItem: What?" && listBoxNames.Items.GetItemAt(1).ToString() == "System.Windows.Controls.ListBoxItem: Where?" && listBoxNames.Items.GetItemAt(2).ToString() == "System.Windows.Controls.ListBoxItem: When?" && listBoxNames.Items.GetItemAt(3).ToString() == "System.Windows.Controls.ListBoxItem: Who?"))
                {
                    listBoxNames.Items.RemoveAt(listBoxNames.SelectedIndex);

                    File.Delete("Saves\\" + ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString() + ".xml");

                    buttonSave_Click(sender, new RoutedEventArgs());
                }
                else
                {
                    MessageBox.Show("Can't delete the Default-Preset!!", "Default-Preset!!!");
                }
            }

            try
            {
                listBoxNames.SelectedIndex = temp - 1;
            }
            catch
            {
            }
        }

        private void buttonPreAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!textBoxPre.Text.Equals(""))
            {
                XmlWriterSettings setting4 = new XmlWriterSettings();
                setting4.Indent = true;
                setting4.ConformanceLevel = ConformanceLevel.Auto;
                try
                {
                    XmlWriter writer4 = XmlWriter.Create("Saves\\" + textBoxPre.Text + ".xml", setting4);
                    listBoxPre.Items.Add(new ListBoxItem() { Content = textBoxPre.Text });
                    writer4.WriteStartDocument();
                    writer4.WriteStartElement("namelist");
                    writer4.WriteEndElement();
                    writer4.WriteEndDocument();
                    writer4.Close();
                }
                catch (Exception eee)
                {
                    if (eee is IOException)
                    {
                        MessageBox.Show("Another user is already using this file.");
                    }
                    else
                    {
                        MessageBox.Show("Other Exception!");
                    }
                }
            }
            listBoxPre.SelectedIndex = 0;
            textBoxPre.Clear();
            textBoxPre.Focus();
        }

        private void buttonPreDel_Click(object sender, RoutedEventArgs e)
        {
            del = 1;
            if (!(listBoxPre.SelectedIndex == 0))
            {
                string tempor = (((ListBoxItem)listBoxPre.SelectedItem).Content.ToString());
                listBoxPre.Items.RemoveAt(listBoxPre.SelectedIndex);
                listBoxPre.SelectedIndex = 0;
                File.Delete("Saves\\" + tempor + ".xml");
            }
            else
            {
                MessageBox.Show("Can't delete the Default-Preset!!", "Default-Preset!!!");
                del = 0;
            }
            del = 0;

            listBoxPre.SelectedIndex = 0;

            XmlDocument doc5 = new XmlDocument();

            try
            {
                listBoxNames.Items.Clear();

                doc5.Load("Saves\\" + ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString() + ".xml");
                XmlElement root = doc5.DocumentElement;
                foreach (XmlNode name in root.ChildNodes)
                {
                    listBoxNames.Items.Add(new ListBoxItem { Content = name.InnerText });
                }
            }
            catch (Exception ee)
            {
                if (ee is IOException)
                {
                    MessageBox.Show("Another user is already using this file.");
                }
                else
                {
                    MessageBox.Show("Other Exception!");
                }
            }
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            XmlWriterSettings setting3 = new XmlWriterSettings();
            setting3.Indent = true;
            setting3.ConformanceLevel = ConformanceLevel.Auto;
            int precount = listBoxPre.Items.Count;
            int namecount = listBoxNames.Items.Count;

            try
            {
                XmlWriter writer3 = XmlWriter.Create("Saves\\preset.xml", setting3);
                writer3.WriteStartDocument();
                writer3.WriteStartElement("presetlist");
                int temp = listBoxPre.SelectedIndex;

                int test = namecount;

                XmlWriter writer5 = XmlWriter.Create("Saves\\" + ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString() + ".xml", setting3);
                writer5.WriteStartDocument();
                writer5.WriteStartElement("namelist");

                if (namecount != 0)
                {
                    listBoxNames.SelectedIndex = 0;

                    for (int j = 0; j < namecount; j++)
                    {
                        writer5.WriteElementString("name", ((ListBoxItem)listBoxNames.SelectedItem).Content.ToString());
                        listBoxNames.SelectedIndex = listBoxNames.SelectedIndex + 1;
                    }
                }
                writer5.WriteEndElement();
                writer5.WriteEndDocument();
                writer5.Close();

                listBoxPre.SelectedIndex = 0;
                for (int i = 0; i < precount; i++)
                {
                    writer3.WriteElementString("preset", ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString());

                    listBoxPre.SelectedIndex = listBoxPre.SelectedIndex + 1;
                }
                writer3.WriteEndElement();
                writer3.WriteEndDocument();
                writer3.Close();
                listBoxPre.SelectedIndex = temp;
            }
            catch (Exception ee)
            {
                if (ee is IOException)
                {
                    MessageBox.Show("Another user is already using this file.");
                }
                else
                {
                    MessageBox.Show("Other Exception!");
                }
            }
        }

        private void listBoxNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int tempnum = listBoxNames.SelectedIndex;
            listBoxNames.SelectedIndex = tempnum;
        }

        private void listBoxPre_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            buttonChoose_Click(sender, new RoutedEventArgs());
        }

        private void listBoxPre_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (del == 0)
            {
                XmlDocument doc3 = new XmlDocument();

                try
                {
                    listBoxNames.Items.Clear();

                    doc3.Load("Saves\\" + ((ListBoxItem)listBoxPre.SelectedItem).Content.ToString() + ".xml");
                    XmlElement root = doc3.DocumentElement;
                    foreach (XmlNode name in root.ChildNodes)
                    {
                        listBoxNames.Items.Add(new ListBoxItem { Content = name.InnerText });
                    }
                }
                catch (Exception ee)
                {
                    if (ee is IOException)
                    {
                        MessageBox.Show("Another user is already using this file.");
                    }
                    else
                    {
                        MessageBox.Show("Other Exception!");
                    }
                }
                listBoxNames.SelectedIndex = 0;
            }
        }
        private void textBoxNames_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buttonNamesAdd_Click(sender, new RoutedEventArgs());
            }
        }

        private void textBoxPre_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buttonPreAdd_Click(sender, new RoutedEventArgs());
            }
        }
    }
}