﻿using System;
using System.Windows;

namespace Random
{
    public partial class Random_Number : Window
    {
        public Random_Number()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            MainWindow MainWindow = new MainWindow();
            MainWindow.Show();
            Close();
        }

        private void buttonRoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Int32.Parse(textBoxStart.Text) > Int32.Parse(textBoxEnd.Text))
                {
                    MessageBox.Show("Startvalue can't be bigger than Endvalue!!!", "Wrong Values");
                }
                else
                {
                    System.Random rnd = new System.Random();
                    textBlockOut.Text = (rnd.Next(Int32.Parse(textBoxStart.Text), (Int32.Parse(textBoxEnd.Text) + 1))).ToString();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Wrong Values", "Wrong Values");
            }
        }
    }
}