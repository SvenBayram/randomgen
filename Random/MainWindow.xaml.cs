﻿using System.Windows;

namespace Random
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonRName_Click(object sender, RoutedEventArgs e)
        {
            Random_Names Random_Names = new Random_Names();
            Random_Names.Show();
            Close();
        }

        private void buttonRNum_Click(object sender, RoutedEventArgs e)
        {
            Random_Number Random_Number = new Random_Number();
            Random_Number.Show();
            Close();
        }

        private void buttonPreset_Click(object sender, RoutedEventArgs e)
        {
            Presets Presets = new Presets();
            Presets.Show();
            Close();
        }

        private void buttonQuit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
